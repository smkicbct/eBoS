<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/logout', function() {
    Auth::logout();
    return redirect('/');
})->name('logout');

Route::group(['prefix' => '/admin','middleware' => 'jabatan','jabatan' => 'Administrator'], function () {
    Route::get('/', 'HomeController@index')->name('admin');

    Route::group(['prefix' => '/modals'], function () {
        Route::group(['prefix' => '/point'], function () {
            Route::get('edit/{id}', 'ModalController@editPoint')->name('modal.point.edit');
        });
        Route::group(['prefix' => '/user'], function () {
            Route::get('edit/{id}', 'ModalController@editUser')->name('modal.user.edit');
            Route::get('lihat/{id}','ModalController@getDetailUser')->name('modal.user.detail');
        });
    });

    Route::group(['prefix' => '/profile'], function () {
        Route::get('index', 'CRUD\BiodataController@page')->name('profile.my');
    });

    Route::group(['prefix' => '/point'], function () {
        Route::get('index', 'CRUD\SkorController@page')->name('point');
        Route::group(['prefix' => '/action'], function () {
            Route::post('post', 'CRUD\SkorController@store')->name('point.post');
            Route::post('update/{id}', 'CRUD\SkorController@update')->name('point.update');
            Route::get('destroy/{id}', 'CRUD\SkorController@destroy')->name('point.destroy');
        });
    });

    Route::group(['prefix' => '/user'], function () {
        Route::get('index', 'CRUD\UserController@page')->name('user');
        Route::group(['prefix' => '/action'], function () {
            Route::post('post', 'CRUD\UserController@store')->name('user.post');
            Route::post('update/{id}', 'CRUD\UserController@update')->name('user.update');
            Route::get('destroy/{id}', 'CRUD\UserController@destroy')->name('user.destroy');
        });
    });
});

// Route guru disini
Route::group(['prefix' => '/guru','middleware' => 'jabatan','jabatan' => 'Guru'], function () {
    Route::get('/','GuruController@getIndex')->name('guru.index');

    Route::group(['prefix' => '/lapor'], function () {
        Route::get('index','GuruController@getLapor')->name('guru.lapor.index');
        Route::get('log','GuruController@getLogLapor')->name('guru.lapor.log.index');
        Route::group(['prefix' => '/action'], function () {
            Route::post('post', 'CRUD\CatatanPointController@store')->name('guru.cp.post');
            Route::get('destroy/{id}', 'CRUD\CatatanPointController@destroy')->name('guru.cp.destroy');
            Route::post('update/{id}', 'CRUD\CatatanPointController@update')->name('guru.cp.update');
        });
    });
    Route::group(['prefix' => '/modals'], function () {
        Route::group(['prefix' => '/siswa'], function () {
            Route::get('lihat/{id}','ModalController@getSiswa')->name('guru.modal.user');
        });
    });
});

Route::group(['prefix' => '/siswa','middleware' => 'jabatan','jabatan' => 'Murid'], function () {
    Route::get('/','siswaController@getIndex')->name('siswa.index');
});