<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skor extends Model
{
    protected $table = "skors";
    protected $primaryKey = "id_skors";
    protected $fillable = [
        'jenis','kode','keterangan','ps','sp'
    ];

    public function catatanpoint()
    {
        return $this->hasOne('App\CatataPoint','id_catatan');
    }
}
