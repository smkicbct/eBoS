<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Biodata extends Model
{
    protected $table = "biodatas";
    protected $primaryKey = "id_biodata";
    protected $fillable = [
        'nama','ttl','alamat','jenis_kelamin','telephone'
    ];

    public function users()
    {
        return $this->hasOne('App\User','id_user');
    }

    public function catatanpoint()
    {
        return $this->hasOne('App\CatatanPoint','id_catatan');
    }
}
