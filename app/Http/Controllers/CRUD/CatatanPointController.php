<?php

namespace App\Http\Controllers\CRUD;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CatatanPoint as CP;
use App\Skor;
use Illuminate\Support\Facades\Auth;

class CatatanPointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = CP::all();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $point_helper = new SkorController();
        $p_helper = $point_helper->show($request->point);
        if (is_array($request->bersangkutan) || is_object($request->bersangkutan)){
            foreach($request->bersangkutan as $list_bersangkutan => $list){
                $data = new CP();
                $data->id_biodata = $list;
                $data->id_skors = $request->point;
                $data->id_user = Auth::id();
                $data->catatan = $request->catatan;
                if($p_helper->ps && !$p_helper->sp){
                    $data->point = '-'.$p_helper->ps;
                } else {
                    $data->point = '+'.$p_helper->sp;
                }
                echo $list.$request->catatan.'<br>';
                $data->save();
                // return true;
                // return dd($p_helper);
                // return dd($list);
            }
            // return var_dump($data);
        }
        else{
            $data = new CP();
            $data->id_biodata = $$request->bersangkutan;
            $data->id_skors = $request->point;
            $data->id_user = Auth::id();
            $data->catatan = $request->catatan;
            $data->point = 'ok';
            $data->save();
            return true;
        }
        return redirect()->back();
        // return var_dump($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = CP::findOrFail($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = CP::findOrFail($id);
        $data->id_biodata = $request->id_biodata;
        $data->id_skors = $request->id_skors;
        $data->id_user = Auth::id();
        $data->catatan = $request->catatan;
        $data->point = $point_helper->show($request->id_point);
        $data->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = CP::findOrFail($id);
        $data->delete();
        return redirect()->back();
    }
}
