<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Biodata;
use Illuminate\Support\Facades\Auth;

class GuruController extends Controller
{
    public function getIndex()
    {
        return view('staff.pages.index');
    }
    
    public function getLapor()
    {
        $point_helper = new CRUD\SkorController();
        $user_helper = new CRUD\UserController();
        $data = $user_helper->index()->where('jabatan','Murid');
        // return dd($data);
        // foreach($data as $list){
        //     echo $list->biodatas->nama;
        // }
        return view('staff.pages.lapor',['point' => $point_helper->index(),'user' => $data]);
    }

    public function getLogLapor()
    {
        $cp_helper = new CRUD\CatatanPointController();
        $data = $cp_helper->index()->where('id_user',Auth::id());
        return view('staff.pages.log_lapor',['log' => $data]);
    }
}
