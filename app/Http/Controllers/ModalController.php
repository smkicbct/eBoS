<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ModalController extends Controller
{
    public function editPoint($id)
    {
        $point_helper = new CRUD\SkorController();
        return view('administrator.pages.modals.edit_point',['data' => $point_helper->show($id)]);
    }

    public function editUser($id)
    {
        $user_helper = new CRUD\UserController();
        return view('administrator.pages.modals.edit_user',['data' => $user_helper->show($id)]);
    }
    
    public function getSiswa($id)
    {
        $user_helper = new CRUD\UserController();
        return view('staff.pages.modals.lihat_detail_user',['data' => $user_helper->show($id)]);
    }

    public function getDetailUser($id)
    {
        $user_helper = new CRUD\UserController();
        return view('administrator.pages.modals.lihat_detail_user',['data' => $user_helper->show($id)]);
    }
}
