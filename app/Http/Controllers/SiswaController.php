<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SiswaController extends Controller
{
    public function aritmathicSkors($sp,$ps,$point = 100)
    {
        if($sp == 0 && $ps){
            $point -= $ps;
            // echo $point.'<br>';
        } else if ($sp && $ps == 0) {
            $point += $sp;
            // echo $point.'<br>';
        }
        return $point;
        
    }
    public function selectAritmathic($ps,$sp)
    {
        if($ps && !$sp){
            return $this->aritmathicSkors(0,$ps);
        } else {
            return $this->aritmathicSkors($sp,0);
        }
    }
    public function getIndex()
    {
        $catatan_helper = new CRUD\CatatanPointController();
        $data = $catatan_helper->index()->where('id_biodata',Auth::user()->id_biodata);
        $skors_helper = new CRUD\SkorController();
        foreach($data as $value => $list){
            $data_skors = $skors_helper->show($data[$value]->id_skors);
            // echo $this->selectAritmathic($data_skors->ps,$data_skors->sp);
        }
        return view('siswa.pages.index',['data' => $data]);
        // dd($data_skors);
        // echo $this->selectAritmathic($data_skors->ps,$data_skors->sp);
        // dd($data);
    }
}
