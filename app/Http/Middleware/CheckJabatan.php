<?php

namespace App\Http\Middleware;

use Closure;

class CheckJabatan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() == null) {
            return redirect('/');
        }
        $actions = $request->route()->getAction();
        $jabatan = isset($actions['jabatan']) ? $actions['jabatan'] : null;

        if($request->user()->hasAnyJabatan($jabatan) || !$jabatan) {
            return $next($request);
        }
        return redirect('/');
    }

    /*Untuk Route
    Route::group(['prefix' => '/admin','middleware' =>'jabatan','jabatan'=>'Isi Dengan Jabatan sesuai dengan table : Admin,Guru,Siswa dan dll ' ], function () {

    });

    Untuk Penggunaan Lain , Blade atau Controller 
    if(Auth::user()->hasJabatan('Isi Dengan Jabatan'))
    {
        Tampilan Sesuai Jabatan
    }
    */
}
