<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatatanPoint extends Model
{
    protected $table ='catatan_points';
    protected $primaryKey ='id_catatan';
    protected $fillable = [
        'catatan','point','tanggal'
    ];
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function skors()
    {
        return $this->belongsTo('App\Skor','id_skors');
    }
    public function biodata()
    {
        return $this->belongsTo('App\Biodata','id_biodata');
    }
    
}
