<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_biodata','email', 'password','jabatan'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $primaryKey = "id_user";

    public function biodatas()
    {
        return $this->belongsTo('App\Biodata','id_biodata');
    }

    public function catatanpoint()
    {
        return $this->hasMany('App\CatatanPoint','id_user');
    }

    public function hasAnyJabatan($jabatan)
    {
         if(is_array($jabatan)){
            foreach ($jabatan as $jabatan){
                if ($this->hasJabatan($jabatan)){
                    return true;
                }
            }
        } else {
            if ($this->hasJabatan($jabatan)){
                return true;
            }
        }
        return false;
    }

     public function hasJabatan($jabatan)
    {
        if (Auth::user()->jabatan==$jabatan){
            return true;
        }
        return false;
    }

}
