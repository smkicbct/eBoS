-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2017 at 06:18 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smkicbct`
--

-- --------------------------------------------------------

--
-- Table structure for table `biodatas`
--

CREATE TABLE `biodatas` (
  `id_biodata` int(10) UNSIGNED NOT NULL,
  `nama` varchar(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ttl` varchar(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `biodatas`
--

INSERT INTO `biodatas` (`id_biodata`, `nama`, `ttl`, `alamat`, `jenis_kelamin`, `telephone`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'Rafi', 'Bandung, 12 November 2000', 'Jalan Atlas 7 No 11', 'laki-laki', '089623096431', '', NULL, NULL),
(2, 'Agus', 'Jakarta, 14 Agustus 1999', 'Jalan Raya 7', 'laki-laki', '089985882323', '', NULL, NULL),
(3, 'Afif', 'Bandung, 30 Maret 1998', 'Jalan Hantap 1', 'laki-laki', '089494223334', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `catatan_points`
--

CREATE TABLE `catatan_points` (
  `id_catatan` int(10) UNSIGNED NOT NULL,
  `id_biodata` int(10) UNSIGNED NOT NULL,
  `id_skors` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci,
  `point` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Point Log',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `catatan_points`
--

INSERT INTO `catatan_points` (`id_catatan`, `id_biodata`, `id_skors`, `id_user`, `catatan`, `point`, `created_at`, `updated_at`) VALUES
(54, 3, 8, 2, 'Juara 1', '+10', '2017-09-13 09:06:02', '2017-09-13 09:06:02'),
(55, 3, 7, 2, 'Mabal ketemu di Lucky Square', '-15', '2017-09-13 09:08:21', '2017-09-13 09:08:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_08_29_161119_biodata', 1),
(2, '2017_08_30_000000_create_users_table', 1),
(3, '2017_08_30_190308_skors', 1),
(4, '2017_08_30_191147_catatan_point', 1);

-- --------------------------------------------------------

--
-- Table structure for table `skors`
--

CREATE TABLE `skors` (
  `id_skors` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(90) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ps` int(11) DEFAULT '0',
  `sp` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `skors`
--

INSERT INTO `skors` (`id_skors`, `jenis`, `kode`, `keterangan`, `ps`, `sp`, `created_at`, `updated_at`) VALUES
(6, 'Setiap keterlambatan lebih dari pukul 07.00', 'B.1.1', 'Pembinaan Oleh Guru Piket', 2, NULL, '2017-09-13 09:00:17', '2017-09-13 09:00:17'),
(7, 'Setiap tidak hadir tanpa keterangan dan terbukti bolos/mabal', 'C.1.1', 'Pembinaan oleh WK/Wks', 15, NULL, '2017-09-13 09:00:51', '2017-09-13 09:00:51'),
(8, 'Perlombaan', 'Sekolah', 'Juara 1', NULL, 10, '2017-09-13 09:02:20', '2017-09-13 09:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_biodata` int(10) UNSIGNED DEFAULT NULL,
  `email` varchar(90) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(251) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(251) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jabatan` varchar(251) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `id_biodata`, `email`, `password`, `foto`, `jabatan`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'rafinyadi@gmail.com', '$2y$10$hKzrXRY6G500sc2uGKs0rO0Ih56DUAZ8s1sanzbQV3aKDYX./bOk6', NULL, 'Administrator', 'LJpHrLB1yvCyuIdLauDE1HZX1FQz5Cc3hSZcdBRSnXVGgNxIgQKiyDyWuKFw', '2017-09-05 10:55:45', '2017-09-05 10:55:45'),
(2, 2, 'xgame1945@gmail.com', '$2y$10$hKzrXRY6G500sc2uGKs0rO0Ih56DUAZ8s1sanzbQV3aKDYX./bOk6', NULL, 'Guru', 'nFP40iyDmC5riSdCI5N6Ruet2myaxrAFAh4Le6oFCG85Ajr2RTXdwhpHdtzx', '2017-09-09 08:05:30', '2017-09-09 08:05:30'),
(3, 3, 'siswa@google.co.id', '$2y$10$hKzrXRY6G500sc2uGKs0rO0Ih56DUAZ8s1sanzbQV3aKDYX./bOk6', NULL, 'Murid', NULL, '2017-09-09 08:18:06', '2017-09-09 08:18:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `biodatas`
--
ALTER TABLE `biodatas`
  ADD PRIMARY KEY (`id_biodata`);

--
-- Indexes for table `catatan_points`
--
ALTER TABLE `catatan_points`
  ADD PRIMARY KEY (`id_catatan`),
  ADD KEY `catatan_points_id_biodata_foreign` (`id_biodata`),
  ADD KEY `catatan_points_id_skors_foreign` (`id_skors`),
  ADD KEY `catatan_points_id_user_foreign` (`id_user`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skors`
--
ALTER TABLE `skors`
  ADD PRIMARY KEY (`id_skors`),
  ADD UNIQUE KEY `skors_kode_unique` (`kode`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_biodata_foreign` (`id_biodata`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `biodatas`
--
ALTER TABLE `biodatas`
  MODIFY `id_biodata` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `catatan_points`
--
ALTER TABLE `catatan_points`
  MODIFY `id_catatan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `skors`
--
ALTER TABLE `skors`
  MODIFY `id_skors` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `catatan_points`
--
ALTER TABLE `catatan_points`
  ADD CONSTRAINT `catatan_points_id_biodata_foreign` FOREIGN KEY (`id_biodata`) REFERENCES `biodatas` (`id_biodata`),
  ADD CONSTRAINT `catatan_points_id_skors_foreign` FOREIGN KEY (`id_skors`) REFERENCES `skors` (`id_skors`),
  ADD CONSTRAINT `catatan_points_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_biodata_foreign` FOREIGN KEY (`id_biodata`) REFERENCES `biodatas` (`id_biodata`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
