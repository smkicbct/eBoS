@extends('layouts.app')

@section('content')
<div class="widget-box span6">
    <div class="widget-title"> <span class="icon"> <i class="icon-picture"></i> </span>
        <h5>Personal Information</h5>
    </div>
    <div class="widget-content">
        <ul class="thumbnails">
            <li class="span2"> <a> <img src="/assets/img/gallery/imgbox3.jpg" alt="" > </a>
            <div class="actions"> <a title="" class="" href="#"><i class="icon-pencil"></i></a> <a class="lightbox_trigger" href="img/gallery/imgbox3.jpg"><i class="icon-search"></i></a> </div>
            </li>
        </ul>
    </div>
</div>
@endsection