<div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Tambah Data Point</h3>
    </div>
    <div class="modal-body">
        <p>Silahkan Pilih Kategori...</p>
        <div class="control-group">
            <label for="checkboxes" class="control-label">Kategori</label>
            <div class="controls">
                <div data-toggle="buttons-radio" class="btn-group">
                    <button class="btn btn-danger" name="jenis" type="button" value="Pelanggaran" onChange="mySelector()">Pelanggaran</button>
                    <button class="btn btn-primary" name="jenis" type="button" value="Reward" onChange="mySelector()">Reward</button>
                </div>
            </div>
        </div>

        <p id="demo"></p>

        <form class="form-horizontal" method="POST" action="{{ Route('point.update', $data->id_skors) }}" name="edit" id="edit" novalidate="novalidate">
            {{ csrf_field() }}
            <div class="control-group">
            <label class="control-label">Jenis</label>
            <div class="controls">
                <input type="text" name="jenis" id="jenis" value="{{ $data->jenis }}">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Kode</label>
            <div class="controls">
                <input type="text" name="kode" id="kode" value="{{ $data->kode }}">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Keterangan</label>
            <div class="controls">
                <input type="text" name="keterangan" id="keterangan" value="{{ $data->keterangan }}">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Point Pelanggaran</label>
            <div class="controls">
                <input type="text" name="ps" id="ps" value="{{ $data->ps }}">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Point Reward</label>
            <div class="controls">
                <input type="text" name="sp" id="sp" value="{{ $data->sp }}">
            </div>
            </div>
        </form>
    </div>
    <div class="modal-footer"> 
    <button class="btn btn-success" form="edit">Save</button> 
    <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>