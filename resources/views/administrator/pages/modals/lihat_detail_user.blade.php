
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Detail User</h3>
    </div>
    <div class="modal-body">
        <p>Semua password yang terinput terenkripsi.</p>

        <form class="form-horizontal" method="POST" action="#" name="user" id="user" novalidate="novalidate">
            {{ csrf_field() }}
            <div class="control-group">
            <label class="control-label">Email</label>
            <div class="controls">
                <input type="text" name="email" id="email" value="{{ $data->email }}" disabled>
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Nama</label>
            <div class="controls">
                <input type="text" name="nama" id="nama" value="{{ $data->biodatas->nama }}" disabled>
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Tempat Tanggal Lahir</label>
            <div class="controls">
                <input type="text" name="ttl" id="ttl" value="{{ $data->biodatas->ttl }}" disabled>
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Alamat</label>
            <div class="controls">
                <input type="text" name="alamat" id="alamat" value="{{ $data->biodatas->alamat }}" disabled>
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Telephone</label>
            <div class="controls">
                <input type="text" name="telephone" id="telephone" value="{{ $data->biodatas->telephone }}" disabled>
            </div>
            </div>
        </form>
    </div>
    <div class="modal-footer"> 
    <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>