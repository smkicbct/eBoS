
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Edit User</h3>
    </div>
    <div class="modal-body">
        <p>Semua password yang terinput terenkripsi.</p>

        <form class="form-horizontal" method="POST" action="{{ Route('user.update', $data->id_user) }}" name="user" id="user" novalidate="novalidate">
            {{ csrf_field() }}
            <div class="control-group">
            <label class="control-label">Email</label>
            <div class="controls">
                <input type="text" name="email" id="email" value="{{ $data->email }}">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
                <input type="password" name="password" id="password" placeholder="Terenkripsi">
            </div>
            </div>
            <div class="control-group">
              <label class="control-label">Jabatan</label>
              <div class="controls">
                <label>
                  <input type="radio" name="jabatan" value="Administrator" />
                  Administrator</label>
                <label>
                  <input type="radio" name="jabatan" value="Guru" />
                  Guru</label>
                <label>
                  <input type="radio" name="jabatan" value="Murid" />
                  Murid</label>
              </div>
            </div>
        </form>
    </div>
    <div class="modal-footer"> 
    <button class="btn btn-success" form="user">Save</button> 
    <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>