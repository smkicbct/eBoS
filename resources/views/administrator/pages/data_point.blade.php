@extends('layouts.app')

@section('content')
<div class="widget-box">
    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
        <h5>Data Point</h5>
        <a class="label label-info" data-toggle="modal" data-target="#myModal" role="button">Tambah Point</a>
    </div>
    <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
            <thead>
                <tr>
                    <th>Kode</th>
                    <th>Jenis</th>
                    <th>Keterangan</th>
                    <th>SP</th>
                    <th>PS</th>
                    <th width="2%">Pengaturan</th>
                </tr>
            </thead>
              <tbody>
              @foreach($data as $show)
                <tr class="gradeX">
                  <td class="center">{{ $show->kode }}</td>
                  <td>{{ $show->jenis }}</td>
                  <td>{{ $show->keterangan }}</td>
                  <td class="center">{{ $show->sp }}</td>
                  <td class="center">{{ $show->ps }}</td>
                  <td class="center"><a data-toggle="modal" data-target="#modalEdit" onclick="getEdit({{ $show->id_skors }})"><i class="icon-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ Route('point.destroy', $show->id_skors)}}"><i class="icon-trash"></i></a></td>
                </tr>
                @endforeach
              </tbody>
        </table>
    </div>
</div>
<!-- Modal Edit -->
<div class="modal fade" id="modalEdit" role="dialog">
</div>
<!-- End -->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Tambah Data Point</h3>
    </div>
    <div class="modal-body">
        <p>Silahkan Pilih Kategori...</p>
        <div class="control-group">
            <label for="checkboxes" class="control-label">Kategori</label>
            <div class="controls">
                <div data-toggle="buttons-radio" class="btn-group">
                    <button class="btn btn-danger" name="jenis" type="button" value="Pelanggaran" onChange="mySelector()">Pelanggaran</button>
                    <button class="btn btn-primary" name="jenis" type="button" value="Reward" onChange="mySelector()">Reward</button>
                </div>
            </div>
        </div>

        <p id="demo"></p>

        <form class="form-horizontal" method="POST" action="{{ Route('point.post') }}" name="point" id="point" novalidate="novalidate">
            {{ csrf_field() }}
            <div class="control-group">
            <label class="control-label">Jenis</label>
            <div class="controls">
                <input type="text" name="jenis" id="jenis">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Kode</label>
            <div class="controls">
                <input type="text" name="kode" id="kode">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Keterangan</label>
            <div class="controls">
                <input type="text" name="keterangan" id="keterangan">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Point Pelanggaran</label>
            <div class="controls">
                <input type="text" name="ps" id="ps">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Point Reward</label>
            <div class="controls">
                <input type="text" name="sp" id="sp">
            </div>
            </div>
        </form>
    </div>
    <div class="modal-footer"> 
    <button class="btn btn-success" form="point">Save</button> 
    <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- End Modal -->
@endsection

@section('js')
<script>
    function mySelector() {
        var x = document.getElementById("jenis").value;
        document.getElementById("demo").innerHTML = "You selected: " + x;
    }
    // ga work
    function getEdit(id) {
        $('#modalEdit').html('<center><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"/></center>');
		jQuery.ajax({
			url: "/admin/modals/point/edit/" + id,
			type: "GET",
			success:
                function(data){
                    $('#modalEdit').html(data);
                    console.log("ok edit");
                }
		});
    }
</script>
@endsection