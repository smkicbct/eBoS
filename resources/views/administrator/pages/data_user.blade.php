@extends('layouts.app')

@section('content')
<div class="widget-box">
    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
        <h5>Data User</h5>
        <a class="label label-info" data-toggle="modal" data-target="#myModal" role="button">Tambah User</a>
    </div>
    <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
            <thead>
                <tr>
                    <th>Email</th>
                    <th width="2%">Jabatan</th>
                    <th width="2%">Options</th>
                </tr>
            </thead>
              <tbody>
              @foreach($data as $show)
                <tr class="gradeX">
                  <td class="center"><a data-toggle="modal" data-target="#modalGetDetail" onclick="getDetail({{ $show->id_user }})">{{ $show->email }}</a></td>
                  <td class="center">{{ $show->jabatan }}</td>
                  <td class="center"><a data-toggle="modal" data-target="#modalEdit" onclick="getEdit({{ $show->id_user }})"><i class="icon-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ Route('user.destroy', $show->id_user)}}"><i class="icon-trash"></i></a></td>
                </tr>
                @endforeach
              </tbody>
        </table>
    </div>
</div>
<!-- Modal Edit -->
<div class="modal fade" id="modalEdit" role="dialog">
</div>
<!-- End -->

<!-- Modal Edit -->
<div class="modal fade" id="modalGetDetail" role="dialog">
</div>
<!-- End -->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Tambah Data User</h3>
    </div>
    <div class="modal-body">
        <p>Semua password yang terinput terenkripsi.</p>

        <form class="form-horizontal" method="POST" action="{{ Route('user.post') }}" name="user" id="user" novalidate="novalidate">
            {{ csrf_field() }}
            <div class="control-group">
            <label class="control-label">Email</label>
            <div class="controls">
                <input type="text" name="email" id="email">
            </div>
            </div>
            <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
                <input type="password" name="password" id="password">
            </div>
            </div>
            <div class="control-group">
              <label class="control-label">Jabatan</label>
              <div class="controls">
                <label>
                  <input type="radio" name="jabatan" value="Administrator" />
                  Administrator</label>
                <label>
                  <input type="radio" name="jabatan" value="Guru" />
                  Guru</label>
                <label>
                  <input type="radio" name="jabatan" value="Murid" />
                  Murid</label>
              </div>
            </div>
        </form>
    </div>
    <div class="modal-footer"> 
    <button class="btn btn-success" form="user">Save</button> 
    <a data-dismiss="modal" class="btn" href="#">Cancel</a>
    </div>
</div>
<!-- End Modal -->
@endsection

@section('js')
<script>
    function mySelector() {
        var x = document.getElementById("jenis").value;
        document.getElementById("demo").innerHTML = "You selected: " + x;
    }
    // ga work
    function getEdit(id) {
        $('#modalEdit').html('<center><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"/></center>');
		jQuery.ajax({
			url: "/admin/modals/user/edit/" + id,
			type: "GET",
			success:
                function(data){
                    $('#modalEdit').html(data);
                    console.log("ok edit");
                }
		});
    }

    function getDetail(id) {
        $('#modalGetDetail').html('<center><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"/></center>');
		jQuery.ajax({
			url: "/admin/modals/user/lihat/" + id,
			type: "GET",
			success:
                function(data){
                    $('#modalGetDetail').html(data);
                    console.log("ok edit");
                }
		});
    }
</script>
@endsection