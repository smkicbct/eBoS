<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Lembaga Wali Nanggroe - CMS</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Bootstrap 3.3.7 -->
    <!--<link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">-->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <!--<link rel="stylesheet" href="./assets/dist/css/AdminLTE.min.css">-->
    <!-- DataTables -->
    <link rel="stylesheet" href="./assets/plugins/datatables/dataTables.bootstrap.css">
    <!-- Modal Ajax -->
    <!--<link rel="stylesheet" href="../jquery.modal.css" type="text/css" media="screen" />-->
    <!-- iCheck -->
    <link rel="stylesheet" href="./assets/plugins/iCheck/square/blue.css">
    <link href="../css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="../themes/explorer/theme.css" media="all" rel="stylesheet" type="text/css"/>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        Content Management System
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                        <!-- Menu -->
                         <li><a href="/admin/artikel">Artikel</a></li>
                         <li><a href="/admin/profil">Profil</a></li>
                         <li><a href="/admin/budaya">Budaya</a></li>
                         <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Sejarah <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="/admin/sejarah">Lihat</a>
                                        <a href="/admin/kategori/sejarah">Kategori</a>
                                    </li>
                                </ul>
                         </li>
                         <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Dokumentasi <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="/admin/gallery">Gallery</a>
                                        <a href="/admin/album">Album</a>
                                    </li>
                                </ul>
                         </li>
                         <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    Pustaka <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="/admin/pustaka">Lihat</a>
                                        <a href="/admin/kategori">Kategori</a>
                                    </li>
                                </ul>
                         </li>
                         <!-- Menu -->

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- jQuery 2.2.3 -->
    <script src="./assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <!--<script src="./assets/bootstrap/js/bootstrap.min.js"></script>-->
    <!-- iCheck -->
    <script src="./assets/plugins/iCheck/icheck.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <!-- File Input -->
    <script src="../js/plugins/sortable.js" type="text/javascript"></script>
    <script src="../js/fileinput.js" type="text/javascript"></script>
    <script src="../js/locales/fr.js" type="text/javascript"></script>
    <script src="../js/locales/es.js" type="text/javascript"></script>
    <script src="../themes/explorer/theme.js" type="text/javascript"></script>
    <!-- DataTables -->
    <script src="./../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="./../assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- Ajax Modal -->
    <script src="../highlight/highlight.pack.js" type="text/javascript" charset="utf-8"></script>
    <script src="../js/jquery.modal.js" type="text/javascript" charset="utf-8"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="./assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script>
    // Open modal in AJAX callback
        $('#lihat-ajax').click(function(event) {
        event.preventDefault();
        $.get(this.href, function(html) {
            $(html).appendTo('body').modal();
        });
        });
    $(function () {
        $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
        });
    });
    </script>
    <script>
         $("#gambar").fileinput({
        showUpload: false,
        showCaption: false,
        browseClass: "btn btn-primary btn-mx",
        fileType: "any",
        previewFileIcon: "<i class='fa fa-upload' aria-hidden='true'></i>",
        overwriteInitial: false,
        initialPreviewAsData: true
    });
    </script>
    <script>
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
    });
    </script>
    <script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "responsive": true,
        "autoWidth": false
        });
    });
    
    // $(document).ready(function(){
    //     $('#edit').on('show.bs.modal', function (e) {
    //         var rowid = $(e.relatedTarget).data('id');
    //         //menggunakan fungsi ajax untuk pengambilan data
    //         $.ajax({
    //             type : 'post',
    //             url : '../modal/artikel/lihat',
    //             data :  'rowid='+ rowid,
    //             success : function(data){
    //             $('.fetched-data').html(data);//menampilkan data ke dalam modal
    //             }
    //         });
    //      });
    // });
    </script>
    @yield('js')
</body>
</html>
