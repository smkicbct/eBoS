<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - Staff Panel</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="/assets/css/select2.css" />
    <link rel="stylesheet" href="/assets/css/uniform.css" />
    <link rel="stylesheet" href="/assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="/assets/css/matrix-style.css" />
    <link rel="stylesheet" href="/assets/css/matrix-media.css" />
    <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="/assets/css/jquery.gritter.css" />

</head>
<body>
@yield('js')
    <!--Header-part-->
<div id="header">
  <h1><a href="/">Matrix Admin</a></h1>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome, User!</span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a href="#"><i class="icon-user"></i> My Profile</a></li>
        <li class="divider"></li>
        <li><a href="#"><i class="icon-check"></i> My Tasks</a></li>
        <li class="divider"></li>
        <li><a href="{{ route('logout') }}"><i class="icon-key"></i> Log Out</a></li>
      </ul>
    </li>
    <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span> <span class="label label-important">5</span> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a class="sAdd" title="" href="#"><i class="icon-plus"></i> new message</a></li>
        <li class="divider"></li>
        <li><a class="sInbox" title="" href="#"><i class="icon-envelope"></i> inbox</a></li>
        <li class="divider"></li>
        <li><a class="sOutbox" title="" href="#"><i class="icon-arrow-up"></i> outbox</a></li>
        <li class="divider"></li>
        <li><a class="sTrash" title="" href="#"><i class="icon-trash"></i> trash</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
    <li class=""><a title="" href="{{ Route('logout') }}"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->
<!--start-top-serch-->
<div id="search">
  <input type="text" placeholder="Search here..."/>
  <button type="submit" class="tip-bottom" title="Search"><i class="icon-search icon-white"></i></button>
</div>
<!--close-top-serch-->
<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="active"><a href="{{ Route('admin') }}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Catatan Siswa</span> <span class="label label-important">2</span></a>
      <ul>
        <li><a href="#">Laporkan</a></li>
        <li><a href="#">Log Laporan</a></li>
      </ul>
    </li>
    <li> <a href="charts.html"><i class="icon icon-signal"></i> <span>Charts &amp; graphs</span></a> </li>
    <li class="content"> <span>Monthly Bandwidth Transfer</span>
      <div class="progress progress-mini progress-danger active progress-striped">
        <div style="width: 77%;" class="bar"></div>
      </div>
      <span class="percent">77%</span>
      <div class="stat">21419.94 / 14000 MB</div>
    </li>
    <li class="content"> <span>Disk Space Usage</span>
      <div class="progress progress-mini active progress-striped">
        <div style="width: 87%;" class="bar"></div>
      </div>
      <span class="percent">87%</span>
      <div class="stat">604.44 / 4000 MB</div>
    </li>
  </ul>
</div>
<!--sidebar-menu-->
<!--main-container-part-->
<div id="content">
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <div class="container-fluid">
<!--End-breadcrumbs-->
    @yield('content')
  </div>
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class="row-fluid">
  <div id="footer" class="span12"> 2017 &copy;<a href="#"> SMK ICB CT</a> </div>
</div>

<!--end-Footer-part-->


    <!-- Scripts -->
    
    <script src="/assets/js/jquery.min.js"></script> 
    <script src="/assets/js/jquery.ui.custom.js"></script> 
    <script src="/assets/js/bootstrap.min.js"></script> 
    <script src="/assets/js/jquery.uniform.js"></script> 
    <script src="/assets/js/select2.min.js"></script> 
    <script src="/assets/js/jquery.dataTables.min.js"></script> 
    <script src="/assets/js/matrix.js"></script> 
    <script src="/assets/js/matrix.tables.js"></script>
    <script src="/assets/js/excanvas.min.js"></script> 
    <script src="/assets/js/jquery.flot.min.js"></script> 
    <script src="/assets/js/jquery.flot.resize.min.js"></script> 
    <script src="/assets/js/jquery.peity.min.js"></script> 
    <script src="/assets/js/fullcalendar.min.js"></script> 
    <script src="/assets/js/matrix.dashboard.js"></script> 
    <script src="/assets/js/jquery.gritter.min.js"></script> 
    <script src="/assets/js/matrix.interface.js"></script> 
    <script src="/assets/js/matrix.chat.js"></script> 
    <script src="/assets/js/jquery.validate.js"></script> 
    <script src="/assets/js/matrix.form_validation.js"></script> 
    <script src="/assets/js/jquery.wizard.js"></script> 
    <script src="/assets/js/matrix.popover.js"></script> 
    <script src="/assets/js/Chart.js"></script> 
    @yield('add.js') 

</body>
</html>
