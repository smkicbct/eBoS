@extends('layouts.murid')

@section('content')
<div class="widget-box">
    <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
    <h5>Catatan Point Terbaru</h5>
    </div>
    <div class="widget-content">
    <div class="todo">
        <ul>
        @foreach($data as $list)
        <li class="clearfix">
            <div class="txt"> {{ $list->catatan }} <span class="date badge badge-important">{{ $list->point }}</span></div>
            <div class="pull-right"> <a class="tip" href="#" title="Edit Task"><i class="icon-pencil"></i></a> <a class="tip" href="#" title="Delete"><i class="icon-remove"></i></a> </div>
        </li>
        @endforeach
        </ul>
    </div>
    </div>
</div>

@endsection
