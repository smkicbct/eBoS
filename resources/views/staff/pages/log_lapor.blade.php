@extends('layouts.guru')

@section('content')
<div class="widget-box">
    <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
        <h5>Log Laporan</h5>
        <a class="label label-info" href=" {{ Route('guru.lapor.index') }}">Tambah Point</a>
    </div>
    <div class="widget-content nopadding">
        <table class="table table-bordered data-table">
            <thead>
                <tr>
                    <th>Tanggal</th>
                    <th>Yang Bersangkutan</th>
                    <th>Jenis Point</th>
                    <th>Catatan</th>
                    <th>Point</th>
                    <th width="2%">Pengaturan</th>
                </tr>
            </thead>
              <tbody>
              @foreach($log as $show)
                <tr class="gradeX">
                  <td class="center">{{ $show->created_at }}</td>
                  <td class="center"><a data-toggle="modal" data-target="#modalSiswa" onclick="getSiswa({{ $show->id_user }})">{{ $show->biodata->nama }}</a></td>
                  <td class="center">{{ $show->skors->kode }} - {{ $show->skors->jenis }}</td>
                  <td>{{ $show->catatan }}</td>
                  <td>{{ $show->point }}</td>
                  <td class="center"><a data-toggle="modal" data-target="#modalEdit" onclick="getEdit({{ $show->id_skors }})"><i class="icon-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ Route('guru.cp.destroy', $show->id_catatan)}}"><i class="icon-trash"></i></a></td>
                </tr>
                @endforeach
              </tbody>
        </table>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade" id="modalSiswa" role="dialog">
</div>
<!-- End -->
@endsection

@section('js')
<script>
    function getSiswa(id) {
        $('#modalSiswa').html('<center><img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif"/></center>');
		jQuery.ajax({
			url: "/guru/modals/siswa/lihat/" + id,
			type: "GET",
			success:
                function(data){
                    $('#modalSiswa').html(data);
                    console.log("ok edit");
                }
		});
    }
</script>
@endsection