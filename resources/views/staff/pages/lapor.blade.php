@extends('layouts.guru')

@section('content')
<div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-list"></i> </span>
    <h5>Input Catatan</h5>
    </div>
    <div class="widget-content"> 
    <form class="form-horizontal" method="POST" action="{{ Route('guru.cp.post') }}" name="user" id="user" accept-charset="UTF-8" novalidate="novalidate">
            {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Point</label>
              <div class="controls">
                <select name="point">
                @foreach($point as $data)
                  <option value="{{ $data->id_skors }}">{{ $data->jenis }}</option>
                @endforeach
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Siswa yang bersangkutan</label>
              <div class="controls">
                <select name="bersangkutan[]" multiple>
                @foreach($user as $data)
                  <option value="{{ $data->id_user }}">{{ $data->biodatas->nama }}</option>
                @endforeach
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Catatan</label>
              <div class="controls">
                <textarea name="catatan"></textarea>
              </div>
            </div>
            <div class="form-actions">
              <button type="submit" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
</div>

@endsection